export const createArray = () =>
  Array(10)
    .fill(false)
    .map(() => Array(10).fill(false))

export const getSelectedAreaCoords = (start, current) => {
  const [startX, startY] = start
  const [currentX, currentY] = current

  return {
    xArea: startX > currentX ? [currentX, startX] : [startX, currentX],
    yArea: startY > currentY ? [currentY, startY] : [startY, currentY],
  }
}
