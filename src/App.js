import './App.css'
import { useState, useEffect } from 'react'

import { createArray, getSelectedAreaCoords } from './helpers'

const initArray = createArray()

function App() {
  const [fields, setFields] = useState(initArray)

  const [isEditMode, setIsEditMode] = useState(false)
  const [isColoredFields, setIsColoredFields] = useState(false)

  const [startPosition, setStartPosition] = useState(null)
  const [currentPosition, setCurrentPosition] = useState(null)

  useEffect(() => {
    if (!isEditMode) return

    const selectedArea = getSelectedAreaCoords(startPosition, currentPosition)

    const newfields = fields.map((subArray, indexY) =>
      subArray.map((field, indexX) =>
        selectedArea.xArea[0] <= indexX &&
        indexX <= selectedArea.xArea[1] &&
        selectedArea.yArea[0] <= indexY &&
        indexY <= selectedArea.yArea[1]
          ? isColoredFields
          : field,
      ),
    )

    setFields(newfields)
  }, [isEditMode, startPosition, currentPosition, isColoredFields])

  const onMouseDown = (xCoords, yCoords, isColored) => {
    setIsEditMode(true)
    setStartPosition([xCoords, yCoords])
    setCurrentPosition([xCoords, yCoords])
    setIsColoredFields(isColored)
  }
  const onMouseUp = (xCoords, yCoords) => {
    setIsEditMode(false)
  }
  const onMouseOver = (xCoords, yCoords) => {
    if (!isEditMode) return
    setCurrentPosition([xCoords, yCoords])
  }

  return (
    <div onMouseLeave={() => setIsEditMode(false)} className="grid">
      {fields.map((subArray, indexY) =>
        subArray.map((field, indexX) => (
          <div
            key={`${indexX} ${indexY}`}
            className={field ? 'field-active' : 'field'}
            onMouseDown={() => onMouseDown(indexX, indexY, !field)}
            onMouseUp={() => onMouseUp(indexX, indexY)}
            onMouseOver={() => onMouseOver(indexX, indexY)}
          />
        )),
      )}
    </div>
  )
}

export default App
